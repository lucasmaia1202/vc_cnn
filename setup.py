import numpy as np
import os
import glob
import time
import random
from sklearn.model_selection import train_test_split as split

np.random.seed(seed=int(time.time()))

rng = np.random.RandomState()

def get_folders(data_base):
  data_folders = []
  for name in os.listdir(data_base):
    if(os.path.isdir(data_base + name)):
      data_folders.append(name)
  print(data_folders)

  return data_folders

def flatten(list_to_flatten):
    for elem in list_to_flatten:
        if isinstance(elem,(list, tuple)):
            for x in flatten(elem):
                yield x
        else:
            yield elem

def config_base(database=None, test_prop=0.0, valid_prop=0.0):

    train_list = []
    y_train_list = []
    test_list = []
    y_test_list = []
    valid_list = []
    y_valid_list = []

    for b in database:
        print("In databaset " + b["url"] + ":")
        folders = get_folders(b["url"])

        label = 0
        for f in folders:
            dataset = glob.glob(b["url"] + f + "/*." + b["img_type"])
            datasize = len(dataset)

            test_num = int(datasize*test_prop)
            valid_num = int(datasize*valid_prop)
            train_num = int(datasize - test_num - valid_num)

            print("In folder " + f + ": " + str(datasize) + " images found.")
            print("Train data: " + str(train_num))
            print("Test data: " + str(test_num))
            print("Valid data: " + str(valid_num))

            X_train, X_test, y_train, y_test = split(dataset, datasize * [label], test_size=test_num, random_state=rng)
            X_train, X_valid, y_train, y_valid = split(X_train, y_train, test_size=valid_num, random_state=rng)

            train_list.append(X_train)
            test_list.append(X_test)
            valid_list.append(X_valid)
            y_train_list.append(y_train)
            y_test_list.append(y_test)
            y_valid_list.append(y_valid)

            print("Data selected...")
            print("Train: " + str(len(X_train)))
            print("Test: " + str(len(X_test)))
            print("Valid: " + str(len(X_valid)))

            label += 1

    train_list = list(flatten(train_list))
    test_list = list(flatten(test_list))
    valid_list = list(flatten(valid_list))
    y_train_list = list(flatten(y_train_list))
    y_test_list = list(flatten(y_test_list))
    y_valid_list = list(flatten(y_valid_list))

    train = list(zip(train_list, y_train_list))
    test = list(zip(test_list, y_test_list))
    valid = list(zip(valid_list, y_valid_list))

    random.shuffle(train)
    random.shuffle(test)
    random.shuffle(valid)

    train_list, y_train_list = zip(*train)
    test_list, y_test_list = zip(*test)
    valid_list, y_valid_list = zip(*valid)

    print("\n\nTOTAL Data selected...")
    print("Train: " + str(len(train_list)))
    print("Test: " + str(len(test_list)))
    print("Valid: " + str(len(valid_list)))
    print(str(len(train_list) + len(test_list) + len(valid_list)) + " Images.")

    return (train_list, y_train_list), (test_list, y_test_list), (valid_list, y_valid_list)
